import React from 'react';

export default function LeftSidebar() {
    return (
        <aside className="left-sidebar">
            <input type="hidden" name="filters" value=""></input>
            <input type="hidden" name="filtersValues" value=""></input>
            <div className="filter">
                Filters
            </div>
        </aside>
    );
  }