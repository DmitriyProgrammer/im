import React from 'react';

export default function NewsBlock() {
  return (
    <div className="news-block"><a href="/im/news/" className="more-news">Все новости</a>
        <p className="title">Новости Ассортимента</p>
        <div className="news-block-col">
            <div className="news-block-col-img"><img width="142px"
                    src="//ipro.etm.ru/upload/news_img/1579512960551.png"></img></div>
            <div className="news-block-col-text"><span className="date">30.01.2020 - </span>
                <h3><a href="/im/news/3725520517">Безгалогенка теперь и в плоском исполнении от завода Конкорд. Когда
                        нужно исключить дымообразование!</a></h3>
                <p>Безгалогенный кабель ППГ-Пнг(А)-HF в плоском исполнении доступен к заказу</p>
            </div>
        </div>
        <div className="news-block-col">
            <div className="news-block-col-img"><img width="142px"
                    src="//ipro.etm.ru/upload/news_img/1579515450453.jpg"></img></div>
            <div className="news-block-col-text"><span className="date">29.01.2020 - </span>
                <h3><a href="/im/news/3725520914">Путаются бухты с проводами? Провод от Экокабель в коробке: удобно
                        хранить, удобно мотать, удобно монтировать!</a></h3>
                <p>Провод силовой ПуВ, ПуГВ в удобной коробке от Экокабель доступен к заказу. Для удобного монтажа.</p>
            </div>
        </div>
    </div>
  );
}