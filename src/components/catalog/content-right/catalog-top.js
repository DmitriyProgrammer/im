import React from 'react';

export default function CatalogTop() {
  return (
    <div className="catalog-top">
        <div className="found">
            Найдено 89 товаров
        </div>
        <div className="catalog-top-select">
            <div className="jq-selectbox jqselect" id="catalog-sort-select-styler"
                style={{display: "inline-block", position: "relative", zIndex: "100"}}><select id="catalog-sort-select"
                    name="sort-select"
                    style={{margin: "0px", padding: "0px", position: "absolute", left: "0px", top: "0px", width: "100%", height: "100%", opacity: "0"}}>
                    <option value="rel" data-sidx="rel" data-sord="desc" selected="selected">По релевантности</option>
                    <option value="name" data-sidx="gds-name" data-sord="asc">По названию</option>
                    <option value="pricer" data-sidx="price" data-sord="desc">По убыванию цены</option>
                    <option value="price" data-sidx="price" data-sord="asc">По возрастанию цены</option>
                </select>
                <div className="jq-selectbox__select" style={{position: "relative"}}>
                    <div className="jq-selectbox__select-text" style={{width: "143px"}}>По релевантности</div>
                    <div className="jq-selectbox__trigger">
                        <div className="jq-selectbox__trigger-arrow"></div>
                    </div>
                </div>
                <div className="jq-selectbox__dropdown" style={{position: "absolute", display: "none"}}>
                    <div className="jq-selectbox__search" style={{display: "none"}}><input type="search" autoComplete="off"
                            placeholder="Поиск..."></input></div>
                    <div className="jq-selectbox__not-found" style={{display: "none"}}>Совпадений не найдено</div>
                    <ul style={{position: "relative", liststyle: "none", overflow: "auto", overflowX: "hidden"}}>
                        <li data-sord="desc" data-sidx="rel" className="selected sel">По релевантности</li>
                        <li data-sord="asc" data-sidx="gds-name" >По названию</li>
                        <li data-sord="desc" data-sidx="price" >По убыванию цены</li>
                        <li data-sord="asc" data-sidx="price" >По возрастанию цены</li>
                    </ul>
                </div>
            </div>
        </div>
        <div className="catalog-top-checkbox">
            <label for="filter_cst">
                <span><input type="checkbox" name="filter_cst" id="filter_cst"></input></span>
                <p>В наличии</p>
            </label>
            <div style={{width:"195px",display:"inline-block",textAlign:"left"}}>
                <div className="jq-selectbox jqselect" id="catalog-shop-select-styler"
                    style={{display: "inline-block", position: "relative", zIndex: "100"}}><select id="catalog-shop-select"
                        name="shop-select"
                        style={{width: "100%", margin: "0px", padding: "0px", position: "absolute", left: "0px", top: "0px", height: "100%", opacity: "0"}}>
                        <option value="0" data-store="0">во всех магазинах</option>
                        <option value="100" data-store="100">г. Санкт-Петербург, ул. Заозерная д.14 </option>
                        <option value="4180" data-store="4180">г. Санкт-Петербург, пр.Энгельса, д. 154 </option>
                        <option value="4190" data-store="4190">г. Санкт-Петербруг, ул. Трефолева, д1. лит.П </option>
                        <option value="4230" data-store="4230">г. Санкт-Петербург, ул. Софийская д. 8 </option>
                        <option value="110" data-store="110">г. Санкт-Петербург, ул. Римского-Корсакова, д. 89 </option>
                        <option value="4120" data-store="4120">г. Санкт-Петербург, ул. Моисеенко, д. 25 </option>
                        <option value="4130" data-store="4130">г. Санкт-Петербург, В.О. Малый пр., д. 75 </option>
                        <option value="4140" data-store="4140">г. Санкт-Петербург, Ленинский пр., д.140 </option>
                        <option value="4150" data-store="4150">г. Санкт-Петербург, Гражданский пр., д. 15 </option>
                        <option value="4170" data-store="4170">г. Санкт-Петербург, Пр. Испытателей, д. 8 кор.1 </option>
                        <option value="4200" data-store="4200">г. Санкт-Петербург, проспект Косыгина, 30 к1</option>
                        <option value="4210" data-store="4210">г. Санкт-Петербург, ул. Савушкина, д.121 кор.1 лит.А
                        </option>
                        <option value="4240" data-store="4240">г. Санкт-Петербург, Ленинский пр., д.90 лит.А, пом.18Н
                        </option>
                        <option value="4250" data-store="4250">г. Санкт-Петербург, пр. Большевиков, д. 25 лит. Д,
                            пом.19-Н </option>
                        <option value="4260" data-store="4260">г.Санкт-Петербург, ул.Ленсовета, д.90, лит.А, пом.2Н
                        </option>
                        <option value="4270" data-store="4270">г. Колпино, п.Тельмана, 2Б ТК 'ОКА-Лента', секция №37
                        </option>
                        <option value="4280" data-store="4280">пос. Шушары, Ленсоветовская дорога, д.12, кор.2</option>
                        <option value="4290" data-store="4290">г. Санкт-Петербург, бульвар Менделеева, д.9 </option>
                        <option value="4300" data-store="4300">г. Санкт-Петербург, пр.Металлистов, д.116 </option>
                        <option value="4310" data-store="4310">пр. Просвещения, д. 76</option>
                        <option value="4320" data-store="4320">г. Санкт-Петербург, пр. Энгельса, д.158 к. 4 стр.1
                        </option>
                        <option value="4330" data-store="4330">г. Гатчина, Пушкинское ш., д.13 к. 2 </option>
                        <option value="4340" data-store="4340">Всеволожск, ул.Плоткина, д.26</option>
                    </select>
                    <div className="jq-selectbox__select" style={{position: "relative"}}>
                        <div className="jq-selectbox__select-text">во всех магазинах</div>
                        <div className="jq-selectbox__trigger">
                            <div className="jq-selectbox__trigger-arrow"></div>
                        </div>
                    </div>
                    <div className="jq-selectbox__dropdown" style={{position: "absolute", width: "408px", display: "none"}}>
                        <div className="jq-selectbox__search"><input type="search" autoComplete="off"
                                placeholder="Поиск..."></input></div>
                        <div className="jq-selectbox__not-found" style={{display: "none"}}>Совпадений не найдено</div>
                        <ul style={{position: "relative", liststyle: "none", overflow: "auto", overflowX: "hidden"}}>
                            <li data-store="0" className="selected sel" >во всех магазинах</li>
                            <li data-store="100" >г. Санкт-Петербург, ул. Заозерная д.14 </li>
                            <li data-store="4180" >г. Санкт-Петербург, пр.Энгельса, д. 154 </li>
                            <li data-store="4190" >г. Санкт-Петербруг, ул. Трефолева, д1. лит.П </li>
                            <li data-store="4230" >г. Санкт-Петербург, ул. Софийская д. 8 </li>
                            <li data-store="110" >г. Санкт-Петербург, ул. Римского-Корсакова, д. 89 </li>
                            <li data-store="4120" >г. Санкт-Петербург, ул. Моисеенко, д. 25 </li>
                            <li data-store="4130" >г. Санкт-Петербург, В.О. Малый пр., д. 75 </li>
                            <li data-store="4140" >г. Санкт-Петербург, Ленинский пр., д.140 </li>
                            <li data-store="4150" >г. Санкт-Петербург, Гражданский пр., д. 15 </li>
                            <li data-store="4170" >г. Санкт-Петербург, Пр. Испытателей, д. 8 кор.1 </li>
                            <li data-store="4200" >г. Санкт-Петербург, проспект Косыгина, 30 к1</li>
                            <li data-store="4210" >г. Санкт-Петербург, ул. Савушкина, д.121 кор.1 лит.А </li>
                            <li data-store="4240" >г. Санкт-Петербург, Ленинский пр., д.90 лит.А, пом.18Н </li>
                            <li data-store="4250" >г. Санкт-Петербург, пр. Большевиков, д. 25 лит. Д, пом.19-Н
                            </li>
                            <li data-store="4260" >г.Санкт-Петербург, ул.Ленсовета, д.90, лит.А, пом.2Н</li>
                            <li data-store="4270" >г. Колпино, п.Тельмана, 2Б ТК 'ОКА-Лента', секция №37 </li>
                            <li data-store="4280" >пос. Шушары, Ленсоветовская дорога, д.12, кор.2</li>
                            <li data-store="4290" >г. Санкт-Петербург, бульвар Менделеева, д.9 </li>
                            <li data-store="4300" >г. Санкт-Петербург, пр.Металлистов, д.116 </li>
                            <li data-store="4310" >пр. Просвещения, д. 76</li>
                            <li data-store="4320" >г. Санкт-Петербург, пр. Энгельса, д.158 к. 4 стр.1 </li>
                            <li data-store="4330" >г. Гатчина, Пушкинское ш., д.13 к. 2 </li>
                            <li data-store="4340" >Всеволожск, ул.Плоткина, д.26</li>
                        </ul>
                    </div>
                </div>
            </div>
            <label for="filter_spec">
                <span><input type="checkbox" name="filter_spec" id="filter_spec"></input></span>
                <p>По акции</p>
            </label>
        </div>
    </div>
  );
}