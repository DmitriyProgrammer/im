import React from 'react';

export default function Brands() {
  return (
    <div className="brands">
        <p className="title">Бренды:</p>
        <span className="all-brands"></span>
        <div className="brands-list">
            <span className="brands-item "
                data-class_and_brand="10000100_elektrokabel_kolchugino_holding_kabelnyy_aljans_hka" data-name="mnf"
                data-charidval="mnf10000100" data-filterparam="id=mnf10000100&amp;name=mnf"
                data-filter-value="mnf10000100" data-nameandid="mnf$mnf10000100">Электрокабель Кольчугино Холдинг
                Кабельный Альянс (ХКА) <span className="brands-counter">(26464)</span></span>
            <span className="brands-item " data-class_and_brand="1795_uglichkabel" data-name="mnf" data-charidval="mnf1795"
                data-filterparam="id=mnf1795&amp;name=mnf" data-filter-value="mnf1795"
                data-nameandid="mnf$mnf1795">Угличкабель (NEXANS) <span className="brands-counter">(11835)</span></span>
            <span className="brands-item " data-class_and_brand="1008_lapp_group" data-name="mnf" data-charidval="mnf1008"
                data-filterparam="id=mnf1008&amp;name=mnf" data-filter-value="mnf1008" data-nameandid="mnf$mnf1008">LAPP
                <span className="brands-counter">(7645)</span></span>
            <span className="brands-item " data-class_and_brand="580_saranskkabel" data-name="mnf" data-charidval="mnf580"
                data-filterparam="id=mnf580&amp;name=mnf" data-filter-value="mnf580"
                data-nameandid="mnf$mnf580">Сарансккабель <span className="brands-counter">(6979)</span></span>
            <span className="brands-item " data-class_and_brand="602_kirskabel" data-name="mnf" data-charidval="mnf602"
                data-filterparam="id=mnf602&amp;name=mnf" data-filter-value="mnf602"
                data-nameandid="mnf$mnf602">КирсКабель <span className="brands-counter">(6886)</span></span>
            <span className="brands-item " data-class_and_brand="594_aljur" data-name="mnf" data-charidval="mnf594"
                data-filterparam="id=mnf594&amp;name=mnf" data-filter-value="mnf594" data-nameandid="mnf$mnf594">АЛЮР
                <span className="brands-counter">(5833)</span></span>
            <span className="brands-item " data-class_and_brand="411_irkutskkabel" data-name="mnf" data-charidval="mnf411"
                data-filterparam="id=mnf411&amp;name=mnf" data-filter-value="mnf411"
                data-nameandid="mnf$mnf411">ИркутскКабель <span className="brands-counter">(4604)</span></span>
            <span className="brands-item " data-class_and_brand="1706_elprom" data-name="mnf" data-charidval="mnf1706"
                data-filterparam="id=mnf1706&amp;name=mnf" data-filter-value="mnf1706"
                data-nameandid="mnf$mnf1706">Элпром <span className="brands-counter">(3613)</span></span>
            <span className="brands-item " data-class_and_brand="927_rosskat" data-name="mnf" data-charidval="mnf927"
                data-filterparam="id=mnf927&amp;name=mnf" data-filter-value="mnf927" data-nameandid="mnf$mnf927">РОССКАТ
                <span className="brands-counter">(2106)</span></span>
            <span className="brands-item " data-class_and_brand="1474_helukabel" data-name="mnf" data-charidval="mnf1474"
                data-filterparam="id=mnf1474&amp;name=mnf" data-filter-value="mnf1474"
                data-nameandid="mnf$mnf1474">HELUKABEL <span className="brands-counter">(2103)</span></span>
            <span className="brands-item " data-class_and_brand="445_konkord" data-name="mnf" data-charidval="mnf445"
                data-filterparam="id=mnf445&amp;name=mnf" data-filter-value="mnf445" data-nameandid="mnf$mnf445">Конкорд
                <span className="brands-counter">(1904)</span></span>
            <span className="brands-item " data-class_and_brand="398_kamkabel" data-name="mnf" data-charidval="mnf398"
                data-filterparam="id=mnf398&amp;name=mnf" data-filter-value="mnf398"
                data-nameandid="mnf$mnf398">Камкабель <span className="brands-counter">(1781)</span></span>
            <span className="brands-item " data-class_and_brand="692_energokabel" data-name="mnf" data-charidval="mnf692"
                data-filterparam="id=mnf692&amp;name=mnf" data-filter-value="mnf692"
                data-nameandid="mnf$mnf692">Энергокабель <span className="brands-counter">(1776)</span></span>
            <span className="brands-item " data-class_and_brand="756_elkab" data-name="mnf" data-charidval="mnf756"
                data-filterparam="id=mnf756&amp;name=mnf" data-filter-value="mnf756" data-nameandid="mnf$mnf756">Элкаб
                <span className="brands-counter">(1769)</span></span>
            <span className="brands-item " data-class_and_brand="1489_sibkabel_holding_kabelnyy_aljans_hka" data-name="mnf"
                data-charidval="mnf1489" data-filterparam="id=mnf1489&amp;name=mnf" data-filter-value="mnf1489"
                data-nameandid="mnf$mnf1489">Сибкабель Холдинг Кабельный Альянс (ХКА) <span
                    className="brands-counter">(1433)</span></span>
            <span className="brands-item " data-class_and_brand="400_ljudinovokabel" data-name="mnf" data-charidval="mnf400"
                data-filterparam="id=mnf400&amp;name=mnf" data-filter-value="mnf400"
                data-nameandid="mnf$mnf400">Людиновокабель <span className="brands-counter">(1312)</span></span>
            <span className="brands-item " data-class_and_brand="1248_segmentenergo" data-name="mnf"
                data-charidval="mnf1248" data-filterparam="id=mnf1248&amp;name=mnf" data-filter-value="mnf1248"
                data-nameandid="mnf$mnf1248">СегментЭнерго <span className="brands-counter">(1307)</span></span>
            <span className="brands-item " data-class_and_brand="771_tomskiy_kabelnyy_zavod" data-name="mnf"
                data-charidval="mnf771" data-filterparam="id=mnf771&amp;name=mnf" data-filter-value="mnf771"
                data-nameandid="mnf$mnf771">Томский кабельный завод <span className="brands-counter">(1267)</span></span>
            <span className="brands-item " data-class_and_brand="397_sevkabel" data-name="mnf" data-charidval="mnf397"
                data-filterparam="id=mnf397&amp;name=mnf" data-filter-value="mnf397"
                data-nameandid="mnf$mnf397">Севкабель <span className="brands-counter">(1252)</span></span>
            <span className="brands-item " data-class_and_brand="1511_ekokabel" data-name="mnf" data-charidval="mnf1511"
                data-filterparam="id=mnf1511&amp;name=mnf" data-filter-value="mnf1511"
                data-nameandid="mnf$mnf1511">Экокабель <span className="brands-counter">(1217)</span></span>
        </div>
        <div className="search-float-top">
            <span className="search-float-close"></span>
            <span className="search-float-text">Найдено товаров: </span>
            <a href="" className="btn-show">Показать</a>
        </div>
    </div>
  );
}