import React from 'react';
import Breadcrumbs from './breadcrumbs.js';
import ShowMobile768 from './show-mobile-768.js';
import BannerCatalog from './banner-catalog.js';
import Brands from './brands.js';
import CatalogTop from './catalog-top.js';
import CatalogContent from './catalog-content.js';
import CatalogBottom from './catalog-bottom.js';
import NewsBlock from './news-block.js';

export default function ContentRight() {
  return (
    <section className="content-right">
        <Breadcrumbs />
        <ShowMobile768 />
        <BannerCatalog />
        <Brands />
        <CatalogTop />
        <CatalogContent />
        <CatalogBottom />
        <NewsBlock />
    </section>
  );
}