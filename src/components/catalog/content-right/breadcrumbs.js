import React from 'react';

export default function Breadcrumbs() {
  return (
    <div className="breadcrumbs" data-gdsclasstreeforemarsys="ИМ: Кабели и провода>ИМ: Кабели>ИМ: Кабели с медной токопроводящей жилой">
        <span>Кабели и провода</span> / <a href="/catalog/1010_kabeli">Кабели</a>
    </div>
  );
}