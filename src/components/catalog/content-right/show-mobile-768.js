import React from 'react';

export default function ShowMobile768() {
  return (
    <div className="show-mobile-768">
        <div className="mobile-catalog">
            <div className="found">
                Найдено 89 товаров
            </div>
            <div className="search-catalog">
                <h3>Найдено в категориях:</h3>
                <ul>
                    <li><a href="/catalog/10101005_sdfkabeli_silovye_dlja_stacionarnoy_prokladki">Кабели силовые для
                            стационарной прокладки CU <span>(66334)</span></a></li>
                    <li><a href="/catalog/10101010_sdfkabeli_silovye_gibkie">Кабели силовые гибкие
                            <span>(6051)</span></a></li>
                    <li><a href="/catalog/10101020_sdfkabeli_kontrolnye">Кабели контрольные <span>(10392)</span></a>
                    </li>
                    <li><a href="/catalog/10101030_sdfkabeli_montazhnye">Кабели монтажные <span>(2172)</span></a></li>
                    <li><a href="/catalog/10101040_sdfkabeli_dlja_signalizacii_i_blokirovki">Кабели для сигнализации и
                            блокировки <span>(857)</span></a></li>
                    <li><a href="/catalog/10101060_sdfkabeli_dlja_telefonii_i_peredachi_dannyh">Кабели для телефонии и
                            передачи данных <span>(4015)</span></a></li>
                    <li><a href="/catalog/10101070_sdfkabeli_sudovye_aviacionnye_i_transportnye">Кабели судовые,
                            авиационные и транспортные <span>(590)</span></a></li>
                    <li><a href="/catalog/10101090_sdfkabeli_specialnogo_naznachenija">Кабели специального назначения
                            <span>(16622)</span></a></li>
                </ul>
            </div>
            <div className="checkbox-mobile-catalog">
                <label for="filter_cst">
                    <span><input type="checkbox" name="filter_cst" id="filter_cst"></input></span>
                    <p>В наличии</p>
                </label>
                <label for="filter_spec">
                    <span><input type="checkbox" name="filter_spec" id="filter_spec"></input></span>
                    <p>По акции</p>
                </label>
                <div className="mobile_up_button">
                    <img src="/public/images/mobile_filters.png"></img>
                    <p>Фильтры</p>
                </div>
            </div>
        </div>
    </div>
  );
}