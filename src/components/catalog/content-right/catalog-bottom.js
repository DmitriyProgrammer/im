import React from 'react';

export default function CatalogBottom() {

    const jqselectStyle = {
        display: "inline-block",
        position: "relative",
        zIndex: "100"
        }
    
    const jqselectboxStyle = {
        margin: "0px",
        padding: "0px",
        position: "absolute",
        left: "0px",
        top: "0px",
        width: "100%",
        height: "100%",
        opacity: "0"
    }

    const jqselectboxselectStyle = {
        position: "relative"
    }

    const jqselectboxselectStyleOne = {
        position: "absolute",
        display: "none"
    }

    const uiOneStyle = {
        position: "relative",
        // list-style: "none",
        overflow: "auto",
        overflowX: "hidden"
    }

  return (
    <div className="catalog-bottom">
        <div className="up"><a href="#top"></a></div>
        <div className="page-nav">
            <span className="active">1</span><a href="/catalog/101010_kabeli_s_mednoy_tokoprovodjaschey_zhiloy?page=2"
                onclick="return false;">2</a><a href="/catalog/101010_kabeli_s_mednoy_tokoprovodjaschey_zhiloy?page=3"
                onclick="return false;">3</a><a href="/catalog/101010_kabeli_s_mednoy_tokoprovodjaschey_zhiloy?page=4"
                onclick="return false;">4</a><a href="/catalog/101010_kabeli_s_mednoy_tokoprovodjaschey_zhiloy?page=5"
                onclick="return false;">5</a><a href="/catalog/101010_kabeli_s_mednoy_tokoprovodjaschey_zhiloy?page=2"
                className="next-page" onclick="return false;"></a>
        </div>
        <div className="show-page">
            <p className="title">На странице:</p>
            <div className="jq-selectbox jqselect" style={jqselectStyle}><select
                    name="goodsOnPage"
                    style={jqselectboxStyle}>
                    <option value="//www.etm.ru/catalog/101010_kabeli_s_mednoy_tokoprovodjaschey_zhiloy"
                        selected="selected">10</option>
                    <option value="//www.etm.ru/catalog/101010_kabeli_s_mednoy_tokoprovodjaschey_zhiloy">15</option>
                    <option value="//www.etm.ru/catalog/101010_kabeli_s_mednoy_tokoprovodjaschey_zhiloy">20</option>
                    <option value="//www.etm.ru/catalog/101010_kabeli_s_mednoy_tokoprovodjaschey_zhiloy">25</option>
                </select>
                <div className="jq-selectbox__select" style={jqselectboxselectStyle}>
                    <div className="jq-selectbox__select-text">10</div>
                    <div className="jq-selectbox__trigger">
                        <div className="jq-selectbox__trigger-arrow"></div>
                    </div>
                </div>
                <div className="jq-selectbox__dropdown" style={jqselectboxselectStyleOne}>
                    {/* <div className="jq-selectbox__search" style={{display: "none"}}><input type="search" autoComplete="off"
                            placeholder="Поиск..."></input></div> */}
                    {/* <div className="jq-selectbox__not-found" style={{display: "none"}}>Совпадений не найдено</div> */}
                    <ul style={uiOneStyle}>
                        <li className="selected sel">10</li>
                        <li>15</li>
                        <li>20</li>
                        <li>25</li>
                    </ul>
                </div>
            </div>
        </div>
        <div className="show-mobile-768">
            <a href="/catalog/101010_kabeli_s_mednoy_tokoprovodjaschey_zhiloy?page=2" className="next-page">
                <div className="btn-more">Показать еще</div>
            </a>
        </div>
    </div>
  );
}