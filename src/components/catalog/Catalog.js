import React from 'react';
import LeftSidebar from './left-sidebar/left-sidebar';
import ContentRight from './content-right/content-right.js';

export default function Catalog() {
  return (
    <section className="main">
        <LeftSidebar />
        <ContentRight />
    </section>
  );
}