import React from 'react';
import Breadcrumbs from './content-right/breadcrumbs.js';
import ContentRight from './content-right/content-right.js';
import CardTop from './content-right/card-top.js';
import ShowMobile768 from './content-right/show-mobile-768.js';
import CardSecondLine from './content-right/card-second-line.js';
import RightProducts from './content-right/right-products.js';
import MainAdventages from './content-right/main-advantages.js';

export default function Card() {
  return (
    <section className="main">
        <Breadcrumbs />
        <h1 itemProp="name">Кабель силовой ВВГнг(А)-FRLSLTx 3х2,5ок (N, PE) -1 (бухта 100м)</h1>
        <ContentRight />
        <CardTop />
        <ShowMobile768 />
        <CardSecondLine />
        <RightProducts />
        <MainAdventages />
    </section>
  );
}