import React from 'react';

export default function CardLeft() {
  return (
    <div className="card-left">
        <div className="card-gallery">
            <div id="sync1" className="big-images owl-carousel owl-theme owl-loaded owl-drag">
                <div className="owl-stage-outer">
                    <div className="owl-stage"
                        style={{transition: "all 0s ease 0s", width: "1800px"}}>
                        <div className="owl-item cloned" style={{width: "360px"}}>
                            <div className="col-slide-card"> <a href="//cdn.etm.ru/ipro/692/vvg-pnf(a)-lsty018.png"
                                    className="slide-card-img" rel="card-gallery-group"> <img
                                        src="//cdn.etm.ru/ipro/692/small_vvg-pnf(a)-lsty018.png"
                                        alt="Кабель силовой ВВГ-Пнг(А)-LS 3х1.5ок(N.PE)-0.66   100м ТРТС"></img> </a></div>
                        </div>
                        <div className="owl-item cloned" style={{width: "360px"}}>
                            <div className="col-slide-card"> <a href="//cdn.etm.ru/ipro/692/vvg-pnf(a)-lsty018.png"
                                    className="slide-card-img" rel="card-gallery-group"> <img
                                        src="//cdn.etm.ru/ipro/692/small_vvg-pnf(a)-lsty018.png"
                                        alt="Кабель силовой ВВГ-Пнг(А)-LS 3х1.5ок(N.PE)-0.66   100м ТРТС"></img> </a></div>
                        </div>
                        <div className="owl-item active" style={{width: "360px"}}>
                            <div className="col-slide-card"> <a href="//cdn.etm.ru/ipro/692/vvg-pnf(a)-lsty018.png"
                                    className="slide-card-img" rel="card-gallery-group"> <img
                                        src="//cdn.etm.ru/ipro/692/small_vvg-pnf(a)-lsty018.png"
                                        alt="Кабель силовой ВВГ-Пнг(А)-LS 3х1.5ок(N.PE)-0.66   100м ТРТС"></img> </a></div>
                        </div>
                        <div className="owl-item cloned" style={{width: "360px"}}>
                            <div className="col-slide-card"> <a href="//cdn.etm.ru/ipro/692/vvg-pnf(a)-lsty018.png"
                                    className="slide-card-img" rel="card-gallery-group"> <img
                                        src="//cdn.etm.ru/ipro/692/small_vvg-pnf(a)-lsty018.png"
                                        alt="Кабель силовой ВВГ-Пнг(А)-LS 3х1.5ок(N.PE)-0.66   100м ТРТС"></img> </a></div>
                        </div>
                        <div className="owl-item cloned" style={{width: "360px"}}>
                            <div className="col-slide-card"> <a href="//cdn.etm.ru/ipro/692/vvg-pnf(a)-lsty018.png"
                                    className="slide-card-img" rel="card-gallery-group"> <img
                                        src="//cdn.etm.ru/ipro/692/small_vvg-pnf(a)-lsty018.png"
                                        alt="Кабель силовой ВВГ-Пнг(А)-LS 3х1.5ок(N.PE)-0.66   100м ТРТС"></img> </a></div>
                        </div>
                    </div>
                </div>
                <div className="owl-nav disabled">
                    <div className="owl-prev"></div>
                    <div className="owl-next"></div>
                </div>
                <div className="owl-dots disabled"></div>
            </div>
            <div id="sync2" className="thumbs owl-carousel owl-theme owl-loaded owl-drag">
                <div className="owl-stage-outer">
                    <div className="owl-stage"></div>
                </div>
                <div className="owl-nav disabled">
                    <div className="owl-prev">prev</div>
                    <div className="owl-next">next</div>
                </div>
                <div className="owl-dots disabled"></div>
            </div>
        </div>
        <div className="card-manuals">
            <a href="//cdn.etm.ru/ipro/692/vvgng_ls.mp4" className="mp4" target="_blank"
                rel="noopener noreferrer">Видеоматериалы</a><a href="//cdn.etm.ru/ipro/692/068.pdf" className="pdf"
                target="_blank" rel="noopener noreferrer">Техническая информация</a><a href="//cdn.etm.ru/ipro/692/069.pdf"
                className="pdf" target="_blank" rel="noopener noreferrer">Каталог производителя</a><a
                href="//cdn.etm.ru/ipro/692/069.pdf" className="pdf" target="_blank" rel="noopener noreferrer">Каталог
                производителя</a>
        </div>
        <div className="left-banners" data-wps-ad="wps_rzcLUDr-a6.1-0"> </div>
        <div className="left-banners" data-wps-ad="wps_rzcLUDr-a6.1-0" style={{backgroundImage: "none"}}>
            <div data-wps-guard="true" style={{position: "relative"}}><img
                    src="https://suite34.emarsys.net/custloads/794071078/md_106820.jpg" data-wps-href="https://ipro.etm.ru/"
                    style={{maxWidth:"100%", maxHeight:"100%"}}></img></div>
        </div>
    </div>
  );
}