import React from 'react';

export default function RightProducts() {
  return (
    <div className="right-products">
        <div className="more-products" style={{display: "none"}}>
        </div>
        <div className="more-products">
            <div className="more-products-top">
                <p>Похожие товары</p>
            </div>
            <div className="tab_container">
                <div className="more-products-st">
                    <div className="more-products-col" data-goodfullid="4630308-0-0">
                        <h3><a href="/cat/nn/4630308/">Кабель силовой ВВГ нг (А) FRLS-LTx 3х2.5 (N.PE)-0.660ТРТС</a></h3>
                        <div className="more-products-col-img"> <img style={{width:"192px"}} alt=""
                                src="//cdn.etm.ru/ipro/1706/small_vvgngfrls3x1.5.png"></img> </div>
                        <div className="left-price">
                            <p className="title">Ваша цена</p> <span className="price">89.64</span>
                        </div>
                        <div className="center-price"></div>
                        <div className="right-price">
                            <p className="title">Розничная цена</p> <span className="price dont-sale">94.3</span>
                        </div> <span className="p-m"> <input type="hidden" className="price" value="89.64"></input> <input type="hidden"
                                className="minMainNumber" value="1"></input> <input type="number" placeholder="" className="requestedNumber"
                                step="1" min="1" value="1" pattern="^[0-9]+$"></input> <span className="modal-input"><span>0.00</span>
                                руб.</span> <span className="sht">м</span> </span> <span className="buy active">В корзину</span>
                        <span className="min_counter_coods">Минимальное количество в заказе 1 м</span> <span
                            className="warning multiplicity">Можно заказать только кратно: 1</span>
                    </div>
                </div>
                <div className="more-products-st">
                    <div className="more-products-col" data-goodfullid="6281394-0-0">
                        <h3><a href="/cat/nn/6281394/">Кабель силовой ВВГнг(А)-FRLSLTx 3х2.5-1 однопр оволочный</a></h3>
                        <div className="more-products-col-img"> <img style={{width:"192px"}} alt=""
                                src="//cdn.etm.ru/ipro/445/small_fr3ok.png"></img> </div>
                        <div className="left-price">
                            <p className="title">Ваша цена</p> <span className="price">96.42</span>
                        </div>
                        <div className="center-price"></div>
                        <div className="right-price">
                            <p className="title">Розничная цена</p> <span className="price dont-sale">101</span>
                        </div> <span className="p-m"> <input type="hidden" className="price" value="96.42"></input> <input type="hidden"
                                className="minMainNumber" value="1"></input> <input type="number" placeholder="" className="requestedNumber"
                                step="1" min="1" value="1" pattern="^[0-9]+$"></input> <span className="modal-input"><span>0.00</span>
                                руб.</span> <span className="sht">м</span> </span> <span className="buy active">В корзину</span>
                        <span className="min_counter_coods">Минимальное количество в заказе 1 м</span> <span
                            className="warning multiplicity">Можно заказать только кратно: 1</span>
                    </div>
                </div>
                <div className="more-products-st">
                    <div className="more-products-col" data-goodfullid="92635-0-0">
                        <h3><a href="/cat/nn/92635/">Кабель силовой ВВГнг(А)-FRLSLTx 3х2.5 (N.PE)-0.660 однопроволочный
                                (барабан)</a></h3>
                        <div className="more-products-col-img"> <img style={{width:"192px"}} alt=""
                                src="//cdn.etm.ru/ipro/594/small_frltx3ok.jpg"></img> </div>
                        <div className="left-price">
                            <p className="title">Ваша цена</p> <span className="price">90.31</span>
                        </div>
                        <div className="center-price"></div>
                        <div className="right-price">
                            <p className="title">Розничная цена</p> <span className="price dont-sale">95.1</span>
                        </div> <span className="p-m"> <input type="hidden" className="price" value="90.31"></input> <input type="hidden"
                                className="minMainNumber" value="1"></input> <input type="number" placeholder="" className="requestedNumber"
                                step="1" min="1" value="1" pattern="^[0-9]+$"></input> <span className="modal-input"><span>0.00</span>
                                руб.</span> <span className="sht">м</span> </span> <span className="buy active">В корзину</span>
                        <span className="min_counter_coods">Минимальное количество в заказе 1 м</span> <span
                            className="warning multiplicity">Можно заказать только кратно: 1</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
  );
}