import React from 'react';
import CardLeft from './card-left.js';

export default function ContentRight() {
  return (
    <section className="content-right card" itemScope itemType="//schema.org/Product">
        <CardLeft />
    </section>
  );
}