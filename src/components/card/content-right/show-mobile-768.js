import React from 'react';

export default function ShowMobile768() {
  return (
<div className="show-mobile-768">
						<div className="btn-open">Характеристики</div>
						<div className="card-info open-link">
                            <div className="line-info">
                                <span>Код товара</span>
                                <span>9113278</span>
                            </div>
                            <div className="line-info">
                                <span>Артикул</span>
                                <span>4353</span>
                            </div>
                            <div className="line-info">
                                <span>Производитель</span>
                                <span><a href="/brand/445_konkord">Конкорд</a></span>
                            </div>
                            <div className="line-info">
                                <span>Страна</span>
                                <span>Россия</span>
                            </div>
                            <div className="line-info">
                                <span>Наименование</span>
                                <span>&nbsp;</span>
                            </div>
                            <div className="line-info">
                                <span>Упаковки</span>
                                <span>100 м</span>
                            </div>
                            <div className="line-info">
                                <span>Сертификат</span>
                                <span>RU C-RU.АЮ64.B01910</span>
                            </div>
                            <div className="line-info">    <span>Тип изделия</span>    <span>Кабель</span></div><div className="line-info">    <span>Марка</span>    <span>ВВГнг(А)-FRLSLTx</span></div><div className="hidden-line-info"><div className="line-info">    <span>Количество жил</span>    <span>3</span></div><div className="line-info">    <span>Сечение жилы, мм2</span>    <span>2.5</span></div><div className="line-info">    <span>Напряжение, В</span>    <span>1000</span></div><div className="line-info">    <span>Исполнение</span>    <span>нг-FRLSLTx</span></div><div className="line-info">    <span>Материал оболочки</span>    <span>ПВХ пониженной горючести не токсичный</span></div><div className="line-info">    <span>Материал изоляции</span>    <span>ПВХ пониженной пожарной опасности с низкой токсичностью продуктов горения</span></div><div className="line-info">    <span>Диапазон рабочих температур</span>    <span>от -50 до +50</span></div><div className="line-info">    <span>Наличие защитного покрова</span>    <span>Нет</span></div><div className="line-info">    <span>Наличие экрана</span>    <span>Нет</span></div><div className="line-info">    <span>Конструкция жилы</span>    <span>Однопроволочная</span></div><div className="line-info">    <span>Форма жилы</span>    <span>Круглая</span></div><div className="line-info">    <span>Диаметр, мм</span>    <span>13.5</span></div><div className="line-info">    <span>Масса, кг</span>    <span>291.31</span></div><div className="line-info">    <span>Материал жилы</span>    <span>Медь</span></div><div className="line-info">    <span>Номинальный ток,А</span>    <span>27</span></div><div className="line-info">    <span>Нормативный документ</span>    <span>ТУ 3500-010-12350648-13</span></div><div className="line-info">    <span>Температура монтажа</span>    <span>до -15</span></div><div className="line-info">    <span>Сфера применения</span>    <span>Для стационарной прокладки</span></div><div className="line-info">    <span>Расцветка провода</span>    <span>Гол.,бел., ж/з</span></div><div className="line-info">    <span>Гарантийный срок, мес</span>    <span>36</span></div><div className="line-info">    <span>Минимальный радиус изгиба</span>    <span>7.5 наружных диаметров</span></div><div className="line-info">    <span>Минимальная строительная длина</span>    <span>100 м</span></div><div className="line-info">    <span>Дополнительная информация</span>    <span>Основная тара бухта</span></div></div><div className="all-char"><span>Все характеристики</span></div>
                        </div>
                        <div className="card-manuals">
                            
                        </div>
					</div>
  );
}