import React from 'react';

export default function CardTop() {
  return (
    <div className="card-top">
        <div className="card-info hidden-768">
            <div className="line-info">
                <span>Код товара</span>
                <span>9113278</span>
            </div>
            <div className="line-info">
                <span>Артикул</span>
                <span itemProp="sku">4353</span>
            </div>
            <div className="line-info" itemProp="brand" itemScope="" itemType="//schema.org/Brand">
                <span>Производитель</span>
                <span itemProp="name"><a href="/brand/445_konkord">Конкорд</a></span>
            </div>
            <div className="line-info">
                <span>Страна</span>
                <span>Россия</span>
            </div>
            <div className="line-info">
                <span>Наименование</span>
                <span>&nbsp;</span>
            </div>
            <div className="line-info">
                <span>Упаковки</span>
                <span>100 м</span>
            </div>
            <div className="line-info">
                <span>Сертификат</span>
                <span>RU C-RU.АЮ64.B01910</span>
            </div>
            <div className="line-info"> <span>Тип изделия</span> <span>Кабель</span></div>
            <div className="line-info"> <span>Марка</span> <span>ВВГнг(А)-FRLSLTx</span></div>
            <div className="hidden-line-info">
                <div className="line-info"> <span>Количество жил</span> <span>3</span></div>
                <div className="line-info"> <span>Сечение жилы, мм2</span> <span>2.5</span></div>
                <div className="line-info"> <span>Напряжение, В</span> <span>1000</span></div>
                <div className="line-info"> <span>Исполнение</span> <span>нг-FRLSLTx</span></div>
                <div className="line-info"> <span>Материал оболочки</span> <span>ПВХ пониженной горючести не токсичный</span>
                </div>
                <div className="line-info"> <span>Материал изоляции</span> <span>ПВХ пониженной пожарной опасности с низкой
                        токсичностью продуктов горения</span></div>
                <div className="line-info"> <span>Диапазон рабочих температур</span> <span>от -50 до +50</span></div>
                <div className="line-info"> <span>Наличие защитного покрова</span> <span>Нет</span></div>
                <div className="line-info"> <span>Наличие экрана</span> <span>Нет</span></div>
                <div className="line-info"> <span>Конструкция жилы</span> <span>Однопроволочная</span></div>
                <div className="line-info"> <span>Форма жилы</span> <span>Круглая</span></div>
                <div className="line-info"> <span>Диаметр, мм</span> <span>13.5</span></div>
                <div className="line-info"> <span>Масса, кг</span> <span>291.31</span></div>
                <div className="line-info"> <span>Материал жилы</span> <span>Медь</span></div>
                <div className="line-info"> <span>Номинальный ток,А</span> <span>27</span></div>
                <div className="line-info"> <span>Нормативный документ</span> <span>ТУ 3500-010-12350648-13</span></div>
                <div className="line-info"> <span>Температура монтажа</span> <span>до -15</span></div>
                <div className="line-info"> <span>Сфера применения</span> <span>Для стационарной прокладки</span></div>
                <div className="line-info"> <span>Расцветка провода</span> <span>Гол.,бел., ж/з</span></div>
                <div className="line-info"> <span>Гарантийный срок, мес</span> <span>36</span></div>
                <div className="line-info"> <span>Минимальный радиус изгиба</span> <span>7.5 наружных диаметров</span></div>
                <div className="line-info"> <span>Минимальная строительная длина</span> <span>100 м</span></div>
                <div className="line-info"> <span>Дополнительная информация</span> <span>Основная тара бухта</span></div>
            </div>
            <div className="all-char"><span>Все характеристики</span></div>
        </div>
        <div className="card-offer " data-goodid="9113278" data-goodfullid="9113278-0-0" data-cnt="В наличии">
            <div itemProp="offers" itemScope="" itemType="//schema.org/AggregateOffer">
                <span className="title-price-card">Цена интернет-магазина</span>
                <span className="price">
                    96.42
                    <span className="price_by_once">Цена за 1 м</span>
                </span>
                {/* <meta itemProp="lowPrice" content="96.42">
                <meta itemProp="priceCurrency" content="RUB"> */}
                <div className="other-price">
                    <p className="title-price">Розничная цена</p>
                    <span className="num-price ">101</span>
                    {/* <meta itemProp="price" content="101"> */}
                    <p className="title-price">
                        Мин. Цена
                        {/* <span className="tooltip-price"><span className="tooltip">Цена по максимальной скидке 10%. Предоставляется при накоплении суммы покупки за все время в 20 000 рублей</span></span> */}
                        <span className="tooltip-price"><span className="tooltip"></span></span>
                    </p>
                    <div itemScope="" itemType="//schema.org/PriceSpecification">
                        <span className="num-price min-rub">91.61</span>
                        {/* <meta itemProp="minPrice" content="91.61">
                        <meta itemProp="priceCurrency" content="RUB"> */}
                    </div>
                </div>
            </div>
            <span className="p-m">
                <input type="hidden" className="price" value="96.42"></input>
                <input type="hidden" className="minMainNumber" value="1"></input>
                {/* <input type="text" placeholder="" className="requestedNumber" value="1"></input> */}
                <input type="number" placeholder="" className="requestedNumber" step="1" min="1" value="1" pattern="^[0-9]+$"></input>
                <span className="modal-input"><span>0.00</span> руб.</span>
                <span className="sht">м</span>
            </span>
            <span className="buy active ">В корзину</span>
            <span className="min_counter_coods">Минимальное количество в заказе 1 м</span>
            <span className="warning multiplicity">Можно заказать только кратно: 1</span>
            <div className="clear"></div>
            <span className="availability">Посмотреть наличие</span>
            <br></br>
            <a className="add-my-catalog not_auth"><span className="msg"></span>В Мое избранное</a>
            <br></br>
            <span className="compare">Сравнить</span>
            <br></br>
            <span className="printer"></span>

        </div>
    </div>
  );
}