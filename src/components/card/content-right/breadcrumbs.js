import React from 'react';

export default function Breadcrumbs() {
  return (
    <div className="breadcrumbs" data-gdsclasstreeforemarsys="ИМ: Кабели и провода>ИМ: Кабели>ИМ: Кабели с медной токопроводящей жилой">
        <span>Кабели и провода</span> / 
        <span>Кабели</span> / 
        <a href="/catalog/101010_kabeli_s_mednoy_tokoprovodjaschey_zhiloy">Кабели с медной токопроводящей жилой</a> / 
        <a className="bold" href="/catalog/10101005_kabeli_silovye_dlja_stacionarnoy_prokladki">Кабели силовые для стационарной прокладки CU</a>
    </div>
  );
}