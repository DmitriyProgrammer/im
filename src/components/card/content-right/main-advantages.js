import React from 'react';

export default function MainAdventages() {
  return (
    <div className="main-advantages">
        <div className="col-advantages plus-1">
            <p>Всегда поможем: <br></br><strong>Центр поддержки <br></br>и продаж</strong></p>
        </div>
        <div className="col-advantages plus-2">
            <p>Скидки до 10% +<br></br>баллы до 10%</p>
        </div>
        <div className="col-advantages plus-3">
            <p>Доставка по городу <br></br><strong>от 150 р.</strong></p>
        </div>
        <div className="col-advantages plus-4">
            <p>Получение в 150 <br></br>пунктах выдачи</p>
        </div>
    </div>
  );
}