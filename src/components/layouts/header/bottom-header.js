import React from 'react';

export default function BottomHeader() {
  return (
    <div className="bottom-header">
        <div className="ins">
        <div className="btn-catalog">
        <span className="btn-open-catalog">Каталог товаров</span>
        <div className="catalog-modal">
            <div className="ins">
            <div className="shadow-nav">
        <div className="col-aside-production">
            <span className="category el">Электрика</span>
            <ul>
                <li>
                    <a id="class10" data-class="10" href="/catalog/10_kabeli_i_provoda">Кабели и провода</a>
                </li>
                <li>
                    <a id="class20" data-class="20" href="/catalog/20_izdelija_dlja_elektromontazha">Изделия для электромонтажа</a>
                </li>
                <li>
                    <a id="class40" data-class="40" href="/catalog/40_rozetki_i_vykljuchateli">Розетки и выключатели</a>
                </li>
                <li>
                    <a id="class50" data-class="50" href="/catalog/50_nizkovoltnoe_oborudovanie">Низковольтное оборудование</a>
                </li>
                <li>
                    <a id="class55" data-class="55" href="/catalog/55_schity_i_shkafy">Щиты и шкафы</a>
                </li>
                <li>
                    <a id="class60" data-class="60" href="/catalog/60_obogrev_i_ventiljacija">Обогрев и вентиляция</a>
                </li>
                <li><a id="class70" data-class="70" href="/catalog/70_instrument">Инструмент</a>
                </li>
                <li>
                    <a id="class80" data-class="80" href="/catalog/80_oborudovanie_6_10kv">Оборудование 6-10кВ</a>
                </li>
                <li>
                    <a id="classS0" data-class="S0" href="/catalog/S0_soputstvujuschie_tovary">Сопутствующие товары</a>
                </li>
            </ul>
        </div>
        <div className="col-aside-production">
            <span className="category sv">Светотехника</span>
            <ul>
                <li>
                    <a id="class3012" data-class="3012" href="/catalog/3012_svetilniki_svetodiodnye_led">Светильники светодиодные (LED)</a>
                </li>
                <li>
                    <a id="class3015" data-class="3015" href="/catalog/3015_tradicionnye_svetilniki">Традиционные светильники</a>
                </li>
                <li>
                    <a id="class3021" data-class="3021" href="/catalog/3021_istochniki_sveta_lampy">Источники света (лампы)</a>
                </li>
                <li>
                    <a id="class3031" data-class="3031" href="/catalog/3031_komplektujuschie_i_aksessuary_dlja_svetilnikov">Комплектующие и аксессуары для светильников</a>
                </li>
                <li>
                    <a id="class3041" data-class="3041" href="/catalog/3041_metallokonstrukcii_dlja_ulichnogo_osveschenija">Металлоконструкции для уличного освещения</a>
                </li>
                <li>
                    <a id="class3051" data-class="3051" href="/catalog/3051_sistemy_upravlenija_osvescheniem">Системы управления освещением</a>
                </li>
            </ul>
        </div>
        <div className="col-aside-production li-open">
            <span className="category san">Сантехника</span>
            <ul>
                <li>
                    <a id="classG105" data-class="G105" href="/catalog/G105_truby_fitingi_gibkie_podvodki">Трубы, фитинги, гибкие подводки   </a>
                </li>
                <li>
                    <a id="classG109" data-class="G109" href="/catalog/G109_teploizoljacija">Теплоизоляция    </a>
                </li>
                <li>
                    <a id="classG113" data-class="G113" href="/catalog/G113_zaporno_regulirujuschaja_armatura">Краны шаровые, затворы, задвижки, вентили</a>
                </li>
                <li>
                    <a id="classG117" data-class="G117" href="/catalog/G117_armatura_predohranitelnaja">Предохранительная и запорно-регулирующая арматура</a>
                </li>
                <li>
                    <a id="classG121" data-class="G121" href="/catalog/G121_kollektory_kollektornye_gruppy_shkafy">Коллекторы, коллекторные группы, шкафы     </a>
                </li>
                <li>
                    <a id="classG125" data-class="G125" href="/catalog/G125_vodopodgotovka_i_filtry_dlja_vody">Фильтры для воды и водоподготовка</a>
                </li>
                <li>
                    <a id="classG129" data-class="G129" href="/catalog/G129_kontrolno_izmeritelnye_pribory">Контрольно-измерительные приборы      </a>
                </li>
                <li>
                    <a id="classG133" data-class="G133" href="/catalog/G133_nasosnoe_oborudovanie">Насосное оборудование</a>
                </li>
                <li>
                    <a id="classG137" data-class="G137" href="/catalog/G137_baki_membrannye">Баки мембранные</a>
                </li>
                <li>
                    <a id="classG141" data-class="G141" href="/catalog/G141_kotelnoe_oborudovanie">Котельное оборудование</a>
                </li>
                <li>
                    <a id="classG145" data-class="G145" href="/catalog/G145_vodonagrevateli_boylery">Водонагреватели, бойлеры</a>
                </li>
                <li>
                    <a id="classG149" data-class="G149" href="/catalog/G149_radiatory_konvektory_termoreguljatory">Радиаторы, конвекторы, терморегуляторы</a>
                </li>
                <li>
                    <a id="classG153" data-class="G153" href="/catalog/G153_kanalizacija_vodoslivnaja_armatura_i_drenazh">Канализация, водосливная арматура и дренаж</a>
                </li>
                <li>
                    <a id="classG157" data-class="G157" href="/catalog/G157_instrument_i_soputstvujuschie_tovary">Инструмент и сопутствующие товары     </a>
                </li>
            </ul>
        </div>
        <div className="col-aside-production li-open">
            <span className="category bez">Безопасность</span>
            <ul>
                <li>
                    <a id="class9010" data-class="9010" href="/catalog/9010_ohranno_pozharnaja_signalizacija_ops">Охранно-пожарная сигнализация (ОПС)</a>
                </li>
                <li>
                    <a id="class9020" data-class="9020" href="/catalog/9020_sistemy_opoveschenija_soue">Системы оповещения (СОУЭ)</a>
                </li>
                <li>
                    <a id="class9030" data-class="9030" href="/catalog/9030_sistemy_videonabljudenija_cctv">Системы видеонаблюдения (CCTV)</a>
                </li>
                <li>
                    <a id="class9040" data-class="9040" href="/catalog/9040_sistemy_kontrolja_dostupa_skud">Системы контроля доступа (СКУД)</a>
                </li>
                <li>
                    <a id="class9050" data-class="9050" href="/catalog/9050_domofony_i_peregovornye_ustroystva">Домофоны и переговорные устройства</a>
                </li>
                <li>
                    <a id="class9070" data-class="9070" href="/catalog/9070_shlagbaumy_i_avtomatika_dlja_vorot">Шлагбаумы и автоматика для ворот</a>
                </li>
                <li>
                    <a id="class9075" data-class="9075" href="/catalog/9075_bloki_pitanija_i_akb">Блоки питания и АКБ</a>
                </li>
                <li>
                    <a id="class9082" data-class="9082" href="/catalog/9082_servery_monitory_komplektujuschie_pk">Серверы, мониторы, комплектующие ПК</a>
                </li>
                <li>
                    <a id="class9090" data-class="9090" href="/catalog/9090_oborudovanie_universalnogo_naznachenija">Автоматизация зданий и Телефония</a>
                </li>
                <li>
                    <a id="class9095" data-class="9095" href="/catalog/9095_kabelno_provodnikovaja_produkcija">Кабельно-проводниковая продукция</a>
                </li>
                <li>
                    <a id="class95" data-class="95" href="/catalog/95_telekommunikacionnoe_oborudovanie_i_sks">Телекоммуникационное оборудование и СКС</a>
                </li>
            </ul>
        </div>
        <div className="col-aside-production">
            <a id="classF0" data-class="F0" href="/catalog/F0_krepezh_i_metizy" className="category kr">Крепеж</a>
        </div>
            </div>
            </div>
        </div>
        </div>
            <div className="bottom-header-right">
            <div className="search">
                <span className="search_ico"></span>
                <input type="text" name="srch" placeholder="Поиск по каталогу товаров" value="" autoComplete="off"></input>
                <input type="submit" value="Поиск"></input>
                <div className="searchSuggestions">
                    <ul className="searchSuggestionsBlock">
                    </ul>
                </div>
            </div>
            <a href="/catalog/?mycat=1" className="chosen-product  "><span className="msg"></span>Мое избранное</a>
            <div className="my-cabinet">
                <span className="btn-my-cabinet "></span>
            </div>
                <a href="/im/basket" className="my-basket">Моя корзина<span>0</span></a>
            </div>
        </div>
    </div>
  );
}
