import React from 'react';

export default function MobileSelectCity() {
  return (
    <div className="mobile-select-city">
      <select name="cities_mobile" id="cities_mobile">
          <option data-class17="ПВАЛЬ" data-store="17" data-class12="16" value="216">Альметьевск</option>
          <option data-class17="СЗАРХ" data-store="11" data-class12="29" value="29">Архангельск</option>
          <option data-class17="ЮГАСТ" data-store="15" data-class12="30" value="30">Астрахань</option>
          <option data-class17="ПВБАл" data-store="13" data-class12="64" value="264">Балаково</option>
          <option data-class17="УРБАР" data-store="16" data-class12="22" value="22">Барнаул</option>
          <option data-class17="ЦНБЛГ" data-store="35" data-class12="31" value="31">Белгород</option>
          <option data-class17="ЦНБРЯ" data-store="19" data-class12="32" value="32">Брянск</option>
          <option data-class17="СЗНВГ" data-store="11" data-class12="53" value="53">Великий Новгород</option>
          <option data-class17="ЦНВЛД" data-store="19" data-class12="33" value="33">Владимир</option>
          <option data-class17="ЮГВОЛ" data-store="15" data-class12="34" value="34">Волгоград</option>
          <option data-class17="ЮГВлг" data-store="15" data-class12="61" value="161">Волгодонск</option>
          <option data-class17="ЮГВЛЖ" data-store="15" data-class12="34" value="134">Волжский</option>
          <option data-class17="СЗВЛГ" data-store="11" data-class12="35" value="35">Вологда</option>
          <option data-class17="ЦНВРН" data-store="35" data-class12="36" value="36">Воронеж</option>
          <option data-class17="СЗВЫБ" data-store="11" data-class12="47" value="47">Выборг</option>
          <option data-class17="ПВДЗЕ" data-store="17" data-class12="52" value="152">Дзержинск</option>
          <option data-class17="УРЕКБ" data-store="12" data-class12="66" value="66">Екатеринбург</option>
          <option data-class17="ЮГЕСТ" data-store="15" data-class12="26" value="126">Ессентуки</option>
          <option data-class17="ЦНЖКВ" data-store="14" data-class12="77" value="477">Жуковский</option>
          <option data-class17="УРЗЛА" data-store="12" data-class12="74" value="274">Златоуст</option>
          <option data-class17="ЦНИвн" data-store="19" data-class12="37" value="37">Иваново</option>
          <option data-class17="ПВИЖЕ" data-store="17" data-class12="18" value="18">Ижевск</option>
          <option data-class17="УРИРК" data-store="16" data-class12="38" value="38">Иркутск</option>
          <option data-class17="ПВИОШ" data-store="17" data-class12="12" value="12">Йошкар-Ола</option>
          <option data-class17="ПВКЗН" data-store="17" data-class12="16" value="16">Казань</option>
          <option data-class17="ЦНКЛГ" data-store="19" data-class12="40" value="40">Калуга</option>
          <option data-class17="УРКЕМ" data-store="16" data-class12="42" value="42">Кемерово</option>
          <option data-class17="ПВКИР" data-store="17" data-class12="43" value="43">Киров</option>
          <option data-class17="ЦНКол" data-store="14" data-class12="77" value="577">Коломна</option>
          <option data-class17="ЦНКос" data-store="19" data-class12="44" value="4432">Кострома</option>
          <option data-class17="ЦНКТЛ" data-store="14" data-class12="77" value="777">Котельники</option>
          <option data-class17="ЮГКРС" data-store="15" data-class12="23" value="23">Краснодар</option>
          <option data-class17="УРКРА" data-store="16" data-class12="24" value="24">Красноярск</option>
          <option data-class17="ЮГКРП" data-store="15" data-class12="23" value="423">Кропоткин</option>
          <option data-class17="УРКрг" data-store="12" data-class12="45" value="45">Курган</option>
          <option data-class17="ЦНКУР" data-store="35" data-class12="46" value="46">Курск</option>
          <option data-class17="ЦНЛИП" data-store="35" data-class12="48" value="48">Липецк</option>
          <option data-class17="УРМАГ" data-store="12" data-class12="74" value="174">Магнитогорск</option>
          <optgroup label="------ Москва: ------"><option data-class17="ЦНМСК" data-store="14" data-class12="77" value="77">Москва</option>
          <option data-class17="ЦНЖКВ" data-store="14" data-class12="77" value="477">Жуковский</option>
          <option data-class17="ЦНКол" data-store="14" data-class12="77" value="577">Коломна</option>
          <option data-class17="ЦНКТЛ" data-store="14" data-class12="77" value="777">Котельники</option>
          <option data-class17="ЦНМТЩ" data-store="14" data-class12="77" value="277">Мытищи</option>
          <option data-class17="ЦНОДН" data-store="14" data-class12="77" value="177">Одинцово</option>
          <option data-class17="ЦНПДЛ" data-store="14" data-class12="77" value="377">Подольск</option>
          <option data-class17="ЦНСпс" data-store="14" data-class12="77" value="677">Сергиев Посад</option>
          <option data-class17="ЦНСрп" data-store="19" data-class12="50" value="5043">Серпухов</option>
          <option data-class17="ЦНХМК" data-store="14" data-class12="77" value="877">Химки</option>
          <option data-class17="ЦНЧех" data-store="19" data-class12="50" value="5048">Чехов</option>
          </optgroup><option data-class17="ЦНМТЩ" data-store="14" data-class12="77" value="277">Мытищи</option>
          <option data-class17="ПВНЧе" data-store="17" data-class12="16" value="116">Набережные Челны</option>
          <option data-class17="УРНЖВ" data-store="12" data-class12="86" value="186">Нижневартовск</option>
          <option data-class17="ПВНКМ" data-store="17" data-class12="16" value="316">Нижнекамск</option>
          <option data-class17="ЦННВГ" data-store="17" data-class12="52" value="52">Нижний Новгород</option>
          <option data-class17="УРНТг" data-store="12" data-class12="66" value="166">Нижний Тагил</option>
          <option data-class17="УРНОВ" data-store="16" data-class12="42" value="142">Новокузнецк</option>
          <option data-class17="ЮГНВР" data-store="15" data-class12="23" value="223">Новороссийск</option>
          <option data-class17="УРНВС" data-store="16" data-class12="54" value="54">Новосибирск</option>
          <option data-class17="ЮГНВЧ" data-store="15" data-class12="61" value="261">Новочеркасск</option>
          <option data-class17="ЦНОбн" data-store="19" data-class12="40" value="4025">Обнинск</option>
          <option data-class17="ЦНОДН" data-store="14" data-class12="77" value="177">Одинцово</option>
          <option data-class17="УРОмс" data-store="16" data-class12="55" value="55">Омск</option>
          <option data-class17="ЦНОРЕ" data-store="35" data-class12="57" value="57">Орёл</option>
          <option data-class17="ПВОРЕ" data-store="13" data-class12="56" value="56">Оренбург</option>
          <option data-class17="ПВОРС" data-store="13" data-class12="56" value="156">Орск</option>
          <option data-class17="ПВПЕН" data-store="13" data-class12="58" value="58">Пенза</option>
          <option data-class17="УРПРМ" data-store="12" data-class12="59" value="59">Пермь</option>
          <option data-class17="СЗПТР" data-store="11" data-class12="10" value="10">Петрозаводск</option>
          <option data-class17="ЦНПДЛ" data-store="14" data-class12="77" value="377">Подольск</option>
          <option data-class17="СЗПСК" data-store="11" data-class12="60" value="60">Псков</option>
          <option data-class17="ЮГПЯТ" data-store="15" data-class12="26" value="05">Пятигорск</option>
          <option data-class17="ЮГРСТ" data-store="15" data-class12="61" value="61">Ростов-на-Дону</option>
          <option data-class17="ЦНРЯЗ" data-store="19" data-class12="62" value="62">Рязань</option>
          <option data-class17="ПВСМР" data-store="13" data-class12="63" value="63">Самара</option>
          <option data-class17="СЗСПБ" data-store="11" data-class12="78" value="78">Санкт-Петербург</option>
          <option data-class17="ПВСРН" data-store="13" data-class12="13" value="13">Саранск</option>
          <option data-class17="ПВСРТ" data-store="13" data-class12="64" value="64">Саратов</option>
          <option data-class17="ЦНСпс" data-store="14" data-class12="77" value="677">Сергиев Посад</option>
          <option data-class17="ЦНСрп" data-store="19" data-class12="50" value="5043">Серпухов</option>
          <option data-class17="ЮГСОЧ" data-store="15" data-class12="23" value="123">Сочи</option>
          <option data-class17="ЮГСТА" data-store="15" data-class12="26" value="26">Ставрополь</option>
          <option data-class17="ЦНОСК" data-store="35" data-class12="31" value="131">Старый Оскол</option>
          <option data-class17="ПВСТЕ" data-store="13" data-class12="02" value="102">Стерлитамак</option>
          <option data-class17="УРСУР" data-store="12" data-class12="86" value="86">Сургут</option>
          <option data-class17="ПВСЗР" data-store="13" data-class12="63" value="263">Сызрань</option>
          <option data-class17="ПВСКТ" data-store="17" data-class12="11" value="11">Сыктывкар</option>
          <option data-class17="ЦНТАМ" data-store="35" data-class12="68" value="68">Тамбов</option>
          <option data-class17="ЮГТИХ" data-store="15" data-class12="23" value="523">Тихорецк</option>
          <option data-class17="УРТОБ" data-store="12" data-class12="72" value="172">Тобольск</option>
          <option data-class17="ПВТЛТ" data-store="13" data-class12="63" value="163">Тольятти</option>
          <option data-class17="УРТОМ" data-store="16" data-class12="70" value="70">Томск</option>
          <option data-class17="ЦНТУЛ" data-store="19" data-class12="71" value="71">Тула</option>
          <option data-class17="УРТМН" data-store="12" data-class12="72" value="72">Тюмень</option>
          <option data-class17="ПВУЛЬ" data-store="17" data-class12="73" value="73">Ульяновск</option>
          <option data-class17="ПВУФА" data-store="17" data-class12="02" value="02">Уфа</option>
          <option data-class17="ЦНХМК" data-store="14" data-class12="77" value="877">Химки</option>
          <option data-class17="ПВЧЕБ" data-store="17" data-class12="21" value="21">Чебоксары</option>
          <option data-class17="УРЧЛБ" data-store="12" data-class12="74" value="74">Челябинск</option>
          <option data-class17="СЗЧРП" data-store="11" data-class12="35" value="135">Череповец</option>
          <option data-class17="ЦНЧех" data-store="19" data-class12="50" value="5048">Чехов</option>
          <option data-class17="ЮГШАХ" data-store="15" data-class12="61" value="361">Шахты</option>
          <option data-class17="ПВЭНГ" data-store="13" data-class12="64" value="164">Энгельс</option>
          <option data-class17="ЦНЯРС" data-store="19" data-class12="76" value="76">Ярославль</option>
      </select>
    </div>
  );
}
