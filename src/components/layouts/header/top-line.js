import React from 'react';

export default function TopLine() {
  return (
    <div className="top-line">
        <div className="ins">
            <ul>
                <li><a target="_blank" rel="noopener noreferrer" href="//www.etm.ru/company/about_etm/">О компании</a></li>
                <li><a target="_blank" rel="noopener noreferrer" href="//www.etm.ru/login.php">Сервис iPRO</a></li>
                <li><a target="_blank" rel="noopener noreferrer" href="http://www.electricforum.ru">Электрофорум</a></li>
                <li><a target="_blank" rel="noopener noreferrer" href="//www.etm.ru/events/">Повышение квалификации</a></li>
                <li><a target="_blank" rel="noopener noreferrer" href="//www.etm.ru/company/career/vacancy/">Вакансии</a></li>
                <li><a href="/im/contacts/78">Контакты</a></li>
            </ul>
            <div className="send-order">
                <a data-modal="send_request_modal" className="md-trigger">Оставить заявку</a>
            </div>
        </div>
    </div>
  );
}
