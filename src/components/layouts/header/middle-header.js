import React from 'react';

export default function MiddleHeader() {
  return (
    <div className="middle-header">
        <div className="ins">
            <div className="open-nav menu-btn">
                <i className="burger_ico"></i>
            </div>
            <a href="/im/basket" className="mobile-basket"><span>0</span></a>
            <div className="logo">
                <a href="/im/">
                    <img src="/public/images/logo.png" alt=""></img>
                </a>
            </div>
            <div className="directions with_plumbing_visible">
                <ul>
                    <li className="li-1">Электрика</li>
                    <li className="li-2">Свет</li>
                    <li className="li-3">Крепеж</li>
                    <li className="li-4">Безопасность</li>
                    <li className="li-5">Сантехника</li>
                </ul>
            </div>
            <div className="about-etm">
                <p>Интернет-магазин ЭТМ -<br></br>это более 1 млн. позиций от 480 поставщиков</p>
            </div>
            <div className="address-select">
                <span className="city">Санкт-Петербург</span>
                <a href="/im/contacts/78" className="more-address">Адреса магазинов</a>
            </div>
            <div className="header-contacts">
                <p>Поможем сделать покупку</p>
                <a href="tel:88007751771" className="phone"><span>8 800 </span>775 17 71</a>
                <p className="time"><b>Пн-Пт</b> с 6<span>00</span> до 21<span>00</span></p>
                <p className="time"><b>Сб</b> с 7<span>00</span> до 19<span>00</span></p>
                <p className="time"><b>Вс</b> с 10<span>00</span> до 19<span>00</span></p>
                <p className="time">&nbsp; (московское время)</p>
            </div>
        </div>
    </div>
  );
}
