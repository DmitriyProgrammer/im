import React from 'react';
import FooterNav from './footer/footer-nav';
import SupportFooter from './footer/support-footer';
import RightFooter from './footer/right-footer';
import ShowMobile768 from './footer/show-mobile-768';
import BottomNav from './footer/bottom-nav';
import Copy from './footer/copy';

export default function Footer() {
  return (
    <footer>
        <div className="ins">
            <FooterNav />
            <SupportFooter />
            <RightFooter />
            <ShowMobile768 />
            <BottomNav />
            <Copy />
        </div>
    </footer>
  );
}