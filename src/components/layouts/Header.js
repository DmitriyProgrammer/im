import React from 'react';
import MobileSelectCity from './header/mobile-select-city';
import TopLine from './header/top-line';
import MiddleHeader from './header/middle-header';
import ShowMobile768 from './header/show-mobile-768.js';
import BottomHeader from './header/bottom-header.js'

export default function Header() {
  return (
    <header>
      <MobileSelectCity/>
      <TopLine/>
      <MiddleHeader/>
      <ShowMobile768/>
      <BottomHeader/>
    </header>
  );
}