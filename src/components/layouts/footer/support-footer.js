import React from 'react';

export default function SupportFooter() {
    return (
        <div className="support-footer">
            <p className="title">Центр поддержки и продаж</p>
            <a href="tel:88007751771" className="phone">8 800 775 17 71</a>
            <div className="col-date">
                <p>
                    <span>Пн</span>
                    <span>Вт</span>
                    <span>Ср</span>
                    <span>Чт</span>
                    <span>Пт</span>
                </p>
                <p>6:00 - 21:00</p>
            </div>
            <div className="col-date">
                <p>
                <span>Сб</span> 7:00 - 19:00</p>
            </div>
            <div className="col-date">
                <p>
                    <span>Вс</span> 10:00 - 19:00
                </p>
                <p className="text-time">(московское время)</p>
            </div>
        </div>
    );
  }