import React from 'react';

export default function RightFooter() {
    return (
        <div className="right-footer">
            <div className="logo-footer">
                <img src="/public/images/logo-footer.png" alt=""></img>
            </div>
            <div className="directions">
                <ul>
                    <li className="li-1">Электрика</li>
                    <li className="li-2">Свет</li>
                    <li className="li-3">Крепеж</li>
                    <li className="li-4">Безопасность</li>
                </ul>
            </div>
            <div className="social">
                <p className="title">Мы в социальных сетях</p>
                <div className="social-full">
                    <a target="_blank" rel="noopener noreferrer" href="https://vk.com/etm_company" className="vk"></a>
                    <a target="_blank" rel="noopener noreferrer" href="https://www.facebook.com/etm.ru" className="fb"></a>
                    <a target="_blank" rel="noopener noreferrer" href="https://twitter.com/ETM_company" className="tw"></a>
                    <a target="_blank" rel="noopener noreferrer" href="https://www.youtube.com/channel/UCev9T3Qx_ZRiEM6VN5yc2ww" className="yt"></a>
                    <a target="_blank" rel="noopener noreferrer" href="https://www.instagram.com/etm_company/" className="inst"></a>
                </div>
            </div>
        </div>
    );
  }