import React from 'react';

export default function Copy() {
    return (
        <div className="copy">
            <div className="ins">
                <p>© 2020 Компания ЭТМ — Копирование и использование в коммерческих целях информации на сайте <a href="//www.etm.ru">www.etm.ru</a> допускается только с письменного одобрения Компании ЭТМ. Информация о товарах, их характеристиках и комплектации может содержать неточности</p>
            </div>
        </div>
    );
  }