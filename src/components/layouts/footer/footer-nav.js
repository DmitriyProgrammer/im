import React from 'react';

export default function FooterNav() {
    return (
        <div className="footer-nav">
            <ul>
                <li>
                    <a href="#">Покупателям</a>
                    <ul>
                        <li><a href="/im/how_to_pay">Способ оплаты</a></li>
                        <li><a href="/im/how_to_receive">Доставка</a></li>
                        <li><a href="/im/actions/">Акции</a></li>
                        <li><a href="/im/discounts">Скидки и баллы</a></li>
                        <li><a href="/im/contacts/78">Адреса магазинов</a></li>
                        <li><a href="/im/offer">Договор оферты</a></li>
                    </ul>
                </li>
            </ul>
            <ul>
                <li>
                    <a href="#">Компания ЭТМ</a>
                    <ul>
                        <li><a target="_blank" rel="noopener noreferrer" href="//www.etm.ru/company/about_etm/">О компании</a></li>
                        <li><a target="_blank" rel="noopener noreferrer" href="//www.etm.ru/login.php">Сервис iPRO</a></li>
                        <li><a target="_blank" rel="noopener noreferrer" href="http://www.electricforum.ru">Электрофорум</a></li>
                        <li><a target="_blank" rel="noopener noreferrer" href="//www.etm.ru/company/career/vacancy/">ЭТМ Вакансии</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    );
  }