import React from 'react';

export default function ShowMobile768() {
    return (
        <div className="show-mobile-768">
            <a href="/im/contacts/78" className="link-address">Адреса магазинов</a>
        </div>
    );
  }