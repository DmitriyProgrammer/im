import React from 'react';

export default function BottomNav() {
    return (
        <div className="bottom-nav">
            <ul>
                <li><a target="_blank" rel="noopener noreferrer" href="//www.etm.ru/events/">Повышение квалификации</a></li>
                <li><a href="/im/faq">Часто задаваемые вопросы</a></li>
                <li><a data-modal="find_error_modal" className="md-trigger">Нашли ошибку?</a></li>
            </ul>
        </div>
    );
  }