import React from 'react';
import SidebarCabinet from './sidebar-cabinet/sidebar-cabinet.js';
import ContentRight from './content-right/content-right.js';

export default function Basket() {
  return (
    <section className="main">
        <SidebarCabinet />
        <ContentRight />
    </section>
  );
}