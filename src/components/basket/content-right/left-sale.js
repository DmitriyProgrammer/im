import React from 'react';

export default function LeftSale() {
  return (
    <div class="left-sale">
        <div class="cupons">
            <p class="code-p">
                Код на скидку
                <span class="hover-tooltip">
                    <span class="tooltip">Размер скидки по коду не может быть больше максимально возможной скидки на товар. Итоговый результат применения кода вы увидите на этапе Оплаты.</span>
                </span>
            </p>
            <input type="text" class="input-cupon" placeholder="Введите код на скидку" value=""></input>
            <a href="#" class="add-cupone">Применить</a>
            <span style={{display: "none",position: "relative",left: "0",top: "12px",fontWeight: "bold",paddingBottom: "20px",fontStyle: "italic",color: "#ed1f24",fontSize: "12px",lineHeight: "1.2"}}>Вы уже применили купон на скидку. В заказе допускается использовать только 1 купон на скидку.</span>
        </div>
        <p>Баллы начисляются на стоимость покупки за вычетом суммы скидки по коду.</p>
    </div>
  );
}