import React from 'react';
import BasketLinkBlock from './basket_link_block.js';
import CopyBasketForm from './copy_basket_form.js';
import StepsGetOrder from './steps-get-order.js';
import TableBasket from './table-basket.js';
import LeftSale from './left-sale.js';
import CheckOut from './checkout.js';
import BottomCheckout from './bottom-checkout.js';

export default function ContentRight() {
  return (
    <section className="content-right">
        <div className="step step_1">
            <BasketLinkBlock />
            <CopyBasketForm />
            <StepsGetOrder />
            <TableBasket />
            <LeftSale />
            <CheckOut />
            <BottomCheckout />
        </div>
    </section>
  );
}