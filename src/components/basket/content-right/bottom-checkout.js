import React from 'react';

export default function BottomCheckout() {
  return (
    <div class="bottom-checkout">
        <textarea name="comments" placeholder="Оставить комментарий" id="" cols="30" rows="10"></textarea>
        <div class="bottom-checkout-links">
            <a href="" class="save">
                <span class="span-link">Сохранить смету</span>
                <span class="hover-tooltip"><span class="tooltip">Нажмите, чтобы сохранить содержимое Корзины как Смету для согласования с заказчиком.</span></span>
            </a>
        </div>
        <input type="submit" class="basket-btn" value="Способ получения"></input>
    </div>
  );
}