import React from 'react';

export default function TableBasket() {
  return (
    <div class="table-basket">
        <table>
            <thead>
                <tr>
                    <td>Наименование</td>
                    <td>Количество</td>
                    <td><span class="rub">Стоимость</span></td>
                    <td><span class="rub">Сумма</span></td>
                    <td>Доступность</td>
                    <td>Удалить</td>
                </tr>
            </thead>
            <tbody>
                <tr class="row-data" data-rownumber="1" data-gds="2428670" data-id="0x0000000000981724" data-avail="1 день"
                    data-fullid="2428670-0-0" data-cnt="1 день">
                    <td>
                        <div class="estimate-product-about">
                            <div class="estimate-product-about-img"> <img alt="" width="61"
                                    src="//cdn.etm.ru/ipro/166/small_nbo-raund-mal.png"></img> </div>
                            <div class="estimate-product-about-text">
                                <h3><a href="/cat/nn/2428670/" target="_blank" rel="noopener noreferrer">Светильник
                                        НБО-23-60-001 (Раунд малый) IP44</a></h3>
                                <p>Арт.:1005500564 Код: ETM2428670</p>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="estimate-amount">
                            <div class="catalog-st"> <span class="p-m"> <input type="hidden" class="price" value="176.56"></input>
                                    <input type="hidden" class="minMainNumber" value="1"></input> <input type="number"
                                        placeholder="" class="requestedNumber" step="1" min="1" value="1"
                                        pattern="^[0-9]+$"></input> <span class="modal-input"><span>0</span> руб.</span> </span>
                                <span class="warning multiplicity">Минимальное количество в заказе: 1 шт.</span> </div>
                        </div>
                    </td>
                    <td>
                        <div class="price-estimate">176.56</div>
                    </td>
                    <td>
                        <div class="price-estimate">176.56</div>
                    </td>
                    <td>
                        <p class="">1 день</p>
                    </td>
                    <td class="del"></td>
                </tr>
                <tr class="row-data" data-rownumber="2" data-gds="34031303" data-id="0x0000000000981725" data-avail="1 день"
                    data-fullid="34031303-0-0" data-cnt="1 день">
                    <td>
                        <div class="estimate-product-about">
                            <div class="estimate-product-about-img"> <img alt="" width="61"
                                    src="//cdn.etm.ru/ipro/445/small_nym3ok.png"></img> </div>
                            <div class="estimate-product-about-text">
                                <h3><a href="/cat/nn/34031303/" target="_blank" rel="noopener noreferrer">Кабель силовой NUM
                                        3х1.5</a></h3>
                                <p>Арт.: Код: ETM34031303</p>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="estimate-amount">
                            <div class="catalog-st"> <span class="p-m"> <input type="hidden" class="price" value="31.52"></input>
                                    <input type="hidden" class="minMainNumber" value="1"></input> <input type="number"
                                        placeholder="" class="requestedNumber" step="1" min="1" value="1"
                                        pattern="^[0-9]+$"></input> <span class="modal-input"><span>0</span> руб.</span> </span>
                                <span class="warning multiplicity">Минимальное количество в заказе: 1 м.</span> </div>
                        </div>
                    </td>
                    <td>
                        <div class="price-estimate">31.52</div>
                    </td>
                    <td>
                        <div class="price-estimate">31.52</div>
                    </td>
                    <td>
                        <p class="">1 день</p>
                    </td>
                    <td class="del"></td>
                </tr>
                <tr>
                    <td colspan="6" style={{fontSize: "12px", lineHeight: "1.5"}}>Обратите внимание, срок поставки заказа
                        определяется по товару с наибольшим сроком поставки. Если вы хотите сократить его, найдите замену
                        этому товару в нашем каталоге с меньшим сроком поставки</td>
                </tr>
            </tbody>
        </table>
    </div>
  );
}