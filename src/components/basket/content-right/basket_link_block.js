import React from 'react';

export default function BasketLinkBlock() {
  return (
    <div class="basket_link_block">
        <a href="#" class="save-exl basket_link_item">Сохранить</a>
        <br></br>
        <a href="#" class="copy_basket_link basket_link_item">Отправить ссылку на корзину</a>
        <br></br>
        <a href="#" class="clear_basket_link basket_link_item">Очистить корзину</a>
    </div>
  );
}