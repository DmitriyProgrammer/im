import React from 'react';

export default function CopyBasketForm() {
  return (
    <span class="copy_basket_form">
        <p>Ссылка на корзину:</p>
        <div class="basket_close"></div>
        <input type="text" class="basket_link" autocomplete="off" autocorrect="off" autofocus=""></input>
    </span>
  );
}