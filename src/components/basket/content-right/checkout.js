import React from 'react';

export default function CheckOut() {
  return (
    <div class="checkout">
        <p>Сумма заказа: <span class="summ_without_discount">224.4 руб.</span></p>
        <p>Ваша скидка <span class="current_percent">7</span>%: <span><span class="number_by_percent">16.32</span> руб.</span></p>
        <h2>Товар на <span class="full_summ">208.08</span></h2>
        <div class="balls">
            <h3>Вы получите <span class="ball_count">0</span> баллов за покупку</h3>
        </div>
    </div>
  );
}