import React from 'react';

export default function SidebarCabinet() {
  return (
    <aside className="sidebar-cabinet">
        <div className="cabinet-nav cabinet-nav-blue">
            <ul>
                <li className="active"><a href="/im/basket">Корзина</a></li>
                <li><a href="/im/orders">История заказов</a></li>
                <li><a href="/im/cabinet">Мой кабинет</a></li>
                <li><a href="/im/estimates">Сметы</a></li>
            </ul>
        </div>
        <div className="cabinet-nav">
            <ul>
                <li><a href="/im/actions/">Акции</a></li>
                <li><a href="/im/news/">Новости</a></li>
                <li><a href="/im/discounts">Скидки и баллы</a></li>
                <li><a href="/im/how_to_pay">Оплата</a></li>
                <li><a href="/im/how_to_receive">Получение</a></li>
                <li><a href="/im/contacts/">Контакты</a></li>
                <li><a href="/im/faq">Частые вопросы</a></li>
            </ul>
        </div>
    </aside>
  );
}