import React from 'react';
import logo from './logo.svg';
import './App.css';
import Header from './components/layouts/Header';
import Footer from './components/layouts/Footer';
import Catalog from './components/catalog/Catalog';
import Card from './components/card/Card.js';
import Basket from './components/basket/Basket.js';
import {BrowserRouter, Route} from 'react-router-dom';

function App() {
  return (
    <BrowserRouter>
      <div>
        <Header />
          <Route path='/catalog' component={Catalog} />
          <Route path='/cat' component={Card} />
          <Route path='/basket' component={Basket} />
        <Footer />
      </div>
    </BrowserRouter>
  );
}

export default App;